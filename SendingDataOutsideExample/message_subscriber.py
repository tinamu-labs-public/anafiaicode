import logging
import olympe
import olympe.log
from olympe.controller import Disconnected
from olympe.messages.common.Common import Reboot
from olympe.messages import mission
from olympe.messages.ardrone3.Piloting import TakeOff, Landing
from olympe.messages.ardrone3.PilotingState import FlyingStateChanged


def main():
    
    drone = olympe.Drone("10.202.0.1")
    olympe.log.update_config({"loggers": {"olympe": {"level": "WARN"}}})
    logger = logging.getLogger("olympe")
    
    # load mission file located in same folder as this file
    with drone.mission.from_path("com.parrot.missions.samples.hello.tar.gz") as hello:
        from olympe.airsdk.messages.parrot.missions.samples.hello.Event import CstTestMessage
        logger.warning("imports worked")

        assert drone.connect()
        assert hello.install(allow_overwrite=True)

        assert drone(Reboot() >> Disconnected()).wait()
        logger.warning("reboot worked")

        # Connect to the drone after reboot, load and activate the 'hello' mission
        assert drone.connect(retry=5)
        assert hello.wait_ready(5)
        logger.warning("hello mission ready")
       
        # load and activate the hello mission
        mission_activated = drone(mission.load(uid=hello.uid) >> mission.activate(uid=hello.uid))
        assert mission_activated.wait(), mission_activated.explain()
        logger.warning("mission activated")

        # eventlistener class definition
        class MyCstMessageListener(olympe.EventListener):
            def __init__(self, *args, **kwds):
                super().__init__(*args, **kwds)
                self.cst_arr = [0] * 1000

            @olympe.listen_event(CstTestMessage(_policy="wait"))
            def on_cst_message_changed(self, event, scheduler):
                self.cst_arr = event.args['my_cst_arr']
                print('cst_arr[0] = {}'.format(self.cst_arr[0]))


        with MyCstMessageListener(drone) as flight_listener:
            for i in range(2):
                assert drone(FlyingStateChanged(state="hovering") | (TakeOff() & FlyingStateChanged(state="hovering"))).wait(200).success()
                drone(Landing()).wait(200)
                assert drone(FlyingStateChanged(state="landed")).wait().success()
           

if __name__ == "__main__":
    main()